﻿namespace GameCollege
{
  partial class GameCollegeUI
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      this.button1 = new System.Windows.Forms.Button();
      this.btnTerug = new System.Windows.Forms.Button();
      this.picGameDemo = new System.Windows.Forms.PictureBox();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.picGameDemo)).BeginInit();
      this.SuspendLayout();
      // 
      // label2
      // 
      this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.Location = new System.Drawing.Point(54, 63);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(393, 74);
      this.label2.TabIndex = 37;
      this.label2.Text = "GameCollege Vote";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(60, 9);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(49, 13);
      this.label1.TabIndex = 38;
      this.label1.Text = "Welkom,";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.BackColor = System.Drawing.Color.Transparent;
      this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label3.Location = new System.Drawing.Point(105, 9);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(51, 13);
      this.label3.TabIndex = 39;
      this.label3.Text = "Gregory";
      // 
      // pictureBox1
      // 
      this.pictureBox1.BackgroundImage = global::GameCollege.Properties.Resources.user_icon_0;
      this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.pictureBox1.Location = new System.Drawing.Point(4, 4);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(48, 48);
      this.pictureBox1.TabIndex = 40;
      this.pictureBox1.TabStop = false;
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(63, 26);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(59, 23);
      this.button1.TabIndex = 41;
      this.button1.Text = "uitloggen";
      this.button1.UseVisualStyleBackColor = true;
      // 
      // btnTerug
      // 
      this.btnTerug.Location = new System.Drawing.Point(18, 722);
      this.btnTerug.Name = "btnTerug";
      this.btnTerug.Size = new System.Drawing.Size(75, 23);
      this.btnTerug.TabIndex = 42;
      this.btnTerug.Text = "terug";
      this.btnTerug.UseVisualStyleBackColor = true;
      this.btnTerug.Visible = false;
      this.btnTerug.Click += new System.EventHandler(this.btnTerug_Click);
      // 
      // picGameDemo
      // 
      this.picGameDemo.BackgroundImage = global::GameCollege.Properties.Resources.Bears_thumb_200x200_8767;
      this.picGameDemo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.picGameDemo.Location = new System.Drawing.Point(182, 209);
      this.picGameDemo.Name = "picGameDemo";
      this.picGameDemo.Size = new System.Drawing.Size(124, 124);
      this.picGameDemo.TabIndex = 43;
      this.picGameDemo.TabStop = false;
      this.picGameDemo.Click += new System.EventHandler(this.picGameDemo_Click);
      // 
      // GameCollegeUI
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.picGameDemo);
      this.Controls.Add(this.btnTerug);
      this.Controls.Add(this.button1);
      this.Controls.Add(this.pictureBox1);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.label2);
      this.Name = "GameCollegeUI";
      this.Size = new System.Drawing.Size(482, 751);
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.picGameDemo)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.PictureBox pictureBox1;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.Button btnTerug;
    private System.Windows.Forms.PictureBox picGameDemo;
  }
}
