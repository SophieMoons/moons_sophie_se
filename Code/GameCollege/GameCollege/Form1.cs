﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameCollege
{
  public partial class Form1 : Form
  {
    public Form1()
    {
      InitializeComponent();
      GameCollege model = new GameCollege();
      GameCollegeController controller = new GameCollegeController(model);
      GameCollegeUI view1 = controller.GetGameCollegeUI();
      view1.Location = new System.Drawing.Point(12, 12);
      view1.Size= new System.Drawing.Size(482,751);
      this.Controls.Add(view1);

      #region tests
      /*GameDemoLijst model = new GameDemoLijst();
      GameDemoLijstController controller = new GameDemoLijstController(model);
      GameDemoLijstUI viewB = controller.GetGameDemoLijstUI(model);
      viewB.Location = new System.Drawing.Point(12, 12);
      viewB.Size = new System.Drawing.Size(482, 751);
      this.Controls.Add(viewB);*/ //-> werkt

      /*StudentenLijst model = new StudentenLijst();
      StudentenLijstController controller = new StudentenLijstController(model);
      StudentenLijstUI viewC = controller.GetStudentenLijstUI(model);
      viewC.Location = new System.Drawing.Point(12, 12);
      viewC.Size=new System.Drawing.Size(482, 751);
      this.Controls.Add(viewC);*/
      //-> werkt
      #endregion 
    }

  }
}
