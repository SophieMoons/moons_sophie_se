﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCollege
{
  public class GameDemoLijst
  {
    public const int AANTAL_GAMEDEMOS = 9;

    private GameDemo[] gameDemos = new GameDemo[AANTAL_GAMEDEMOS];
    private List<GameDemoLijstUI> observers = new List<GameDemoLijstUI>(); //voor GameDemoLijstController -> GameCollege

    public GameDemoLijst()
    {
      this.gameDemos = new GameDemo[AANTAL_GAMEDEMOS];

      for (int i = 0; i < GameDemoLijst.AANTAL_GAMEDEMOS; i++)
      {
        this.gameDemos[i]= new GameDemo();
      }
    }

    public GameDemo GetGameDemo(int index)
    {
      return gameDemos[index];
    }

    public void AddObserver(GameDemoLijstUI observer) //Checken
    {
      observers.Add(observer);
    }

    public void LaatStudentenZien(int index) //-> zal GameDemoLijst verbergen
    {
      gameDemos[index].LaatStudentenZien();
    }

    /*private void NotifyObservers()
    {
      // eh...
    }*/

  }
}

