﻿namespace GameCollege
{
  partial class StudentIDUI
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btnStem = new System.Windows.Forms.Button();
      this.lblLeeftijd = new System.Windows.Forms.Label();
      this.lblDevelopmentFunctie = new System.Windows.Forms.Label();
      this.lblVolledigeNaam = new System.Windows.Forms.Label();
      this.picBoxStudent1 = new System.Windows.Forms.PictureBox();
      ((System.ComponentModel.ISupportInitialize)(this.picBoxStudent1)).BeginInit();
      this.SuspendLayout();
      // 
      // btnStem
      // 
      this.btnStem.Location = new System.Drawing.Point(339, 32);
      this.btnStem.Name = "btnStem";
      this.btnStem.Size = new System.Drawing.Size(65, 49);
      this.btnStem.TabIndex = 52;
      this.btnStem.Text = "STEM";
      this.btnStem.UseVisualStyleBackColor = true;
      this.btnStem.Click += new System.EventHandler(this.btnStem_Click);
      // 
      // lblLeeftijd
      // 
      this.lblLeeftijd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblLeeftijd.Location = new System.Drawing.Point(100, 69);
      this.lblLeeftijd.Name = "lblLeeftijd";
      this.lblLeeftijd.Size = new System.Drawing.Size(206, 24);
      this.lblLeeftijd.TabIndex = 51;
      this.lblLeeftijd.Text = "Leeftijd";
      // 
      // lblDevelopmentFunctie
      // 
      this.lblDevelopmentFunctie.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblDevelopmentFunctie.Location = new System.Drawing.Point(100, 45);
      this.lblDevelopmentFunctie.Name = "lblDevelopmentFunctie";
      this.lblDevelopmentFunctie.Size = new System.Drawing.Size(206, 24);
      this.lblDevelopmentFunctie.TabIndex = 50;
      this.lblDevelopmentFunctie.Text = "Functie tijdens dev.";
      // 
      // lblVolledigeNaam
      // 
      this.lblVolledigeNaam.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblVolledigeNaam.Location = new System.Drawing.Point(100, 21);
      this.lblVolledigeNaam.Name = "lblVolledigeNaam";
      this.lblVolledigeNaam.Size = new System.Drawing.Size(206, 24);
      this.lblVolledigeNaam.TabIndex = 49;
      this.lblVolledigeNaam.Text = "Voornaam Naam";
      // 
      // picBoxStudent1
      // 
      this.picBoxStudent1.BackgroundImage = global::GameCollege.Properties.Resources.Login_Student;
      this.picBoxStudent1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.picBoxStudent1.Location = new System.Drawing.Point(15, 21);
      this.picBoxStudent1.Name = "picBoxStudent1";
      this.picBoxStudent1.Size = new System.Drawing.Size(70, 72);
      this.picBoxStudent1.TabIndex = 48;
      this.picBoxStudent1.TabStop = false;
      // 
      // StudentIDUI
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.btnStem);
      this.Controls.Add(this.lblLeeftijd);
      this.Controls.Add(this.lblDevelopmentFunctie);
      this.Controls.Add(this.lblVolledigeNaam);
      this.Controls.Add(this.picBoxStudent1);
      this.Name = "StudentIDUI";
      this.Size = new System.Drawing.Size(416, 112);
      ((System.ComponentModel.ISupportInitialize)(this.picBoxStudent1)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btnStem;
    private System.Windows.Forms.Label lblLeeftijd;
    private System.Windows.Forms.Label lblDevelopmentFunctie;
    private System.Windows.Forms.Label lblVolledigeNaam;
    private System.Windows.Forms.PictureBox picBoxStudent1;
  }
}
