﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCollege
{
  public class GameDemoController
  {
    protected GameDemo gameDemo;

    public GameDemoController(GameDemo model)
    {
      gameDemo = model;
    }

    public GameDemoUI GetGameDemoUI(GameDemo gameDemo)
    {
      GameDemoUI gameDemoUI = new GameDemoUI(gameDemo, this);
      gameDemo.AddObserver(gameDemoUI);
      return gameDemoUI;
    }

    /*public void GameDemoClicked()
    {
      if (this.gameDemo.OpGeklikt())
      {
        this.gameDemo.BlijfOpScherm();
      }
      else
      {
        this.gameDemo.LaatStudentenZien();
      }
    }*/
  }
}
