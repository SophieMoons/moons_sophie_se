﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCollege
{
  public class GameCollege
  {
    private GameDemoLijst[] gameDemoLijst = new GameDemoLijst[1];
    private StudentenLijst[] studentenLijst = new StudentenLijst[1];

    public const int AANTAL_GAMEDEMOLIJSTEN = 1;
    public const int AANTAL_STUDENTENLIJSTEN = 1;

    public GameCollege() //Checken
    {
      for (int i = 0; i < gameDemoLijst.Length; i++)
      {
        gameDemoLijst[i]=new GameDemoLijst();
      }

      for (int i = 0; i < studentenLijst.Length; i++)
      {
        studentenLijst[i]=new StudentenLijst();
      }
    }

    public GameDemoLijst GetGameDemoLijst(int index)
    {
      return gameDemoLijst[index];
    }

    public StudentenLijst GetStudentenLijst(int index)
    {
      return studentenLijst[index];
    }

    /*public void SwitchSchermen() //nieuw
    {
      //zien of dit werkt
      if (studentenLijst[1].BlijfOpScherm())
      {
        gameDemoLijst[1].LaatStudentenZien(1);
      }
    }*/
  }
}
