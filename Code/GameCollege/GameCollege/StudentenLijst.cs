﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCollege
{
  public class StudentenLijst
  {
    private StudentID[] studentID;
    //private int aantalKeerGestemd=0;
    private List<StudentenLijstUI> observers= new List<StudentenLijstUI>();
    private StudentIDController[] studentIDController = new StudentIDController[1];
    //private bool blijfOpScherm = true;

    public const int AANTAL_STUDENTEN = 4; //const -> mogen niet veranderd worden
    //public const int MAX_AANTAL_STEMMEN = 3; // waar ga ik dit zetten...

    public StudentenLijst()
    {
      this.studentID = new StudentID[AANTAL_STUDENTEN];

      for (int i = 0; i < StudentenLijst.AANTAL_STUDENTEN; i++)
      { 
        this.studentID[i]= new StudentID();
      }
    }

    /*public Boolean EndMaxStemmen() //Checken
    {
      return aantalKeerGestemd == MAX_AANTAL_STEMMEN;
    }*/

    /*public bool BlijfOpScherm()
    {
      return blijfOpScherm;
    }*/

    public StudentID GetStudentID(int index)
    {
      return studentID[index];
    }

    public void AddObserver(StudentenLijstUI observer)
    {
      observers.Add(observer);
    }

    /*private void NotifyObservers()
    {
      // eh...
    }*/

    /*public void Stem()
    {
      for (int i = 0; i < studentID.Length; i++)
      {
        if (studentID[i].ErIsGestemd())
        {
          if (aantalKeerGestemd == MAX_AANTAL_STEMMEN)
          {
            for (int j = 0; j < studentID.Length; j++)
            {
              studentID[j].MaakLos();
              studentID[j].StemCheck();
            }
          }
          else
          {
            for (int k = 0; k < studentID.Length; k++)
            {
              studentID[k].MaakVast();
              studentID[k].StemCheck();
            }
            aantalKeerGestemd++;
          }
        }
      }
    }*/
    /*public void TerugGaan()
    {
      blijfOpScherm = false;
    }*/
  }
}
