﻿namespace GameCollege
{
  partial class GameDemoUI
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.picBoxGameDemo1 = new System.Windows.Forms.PictureBox();
      ((System.ComponentModel.ISupportInitialize)(this.picBoxGameDemo1)).BeginInit();
      this.SuspendLayout();
      // 
      // picBoxGameDemo1
      // 
      this.picBoxGameDemo1.BackgroundImage = global::GameCollege.Properties.Resources.GameDemoVB;
      this.picBoxGameDemo1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.picBoxGameDemo1.Location = new System.Drawing.Point(3, 3);
      this.picBoxGameDemo1.Name = "picBoxGameDemo1";
      this.picBoxGameDemo1.Size = new System.Drawing.Size(126, 122);
      this.picBoxGameDemo1.TabIndex = 16;
      this.picBoxGameDemo1.TabStop = false;
      // 
      // GameDemoUI
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.picBoxGameDemo1);
      this.Name = "GameDemoUI";
      this.Size = new System.Drawing.Size(132, 128);
      ((System.ComponentModel.ISupportInitialize)(this.picBoxGameDemo1)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.PictureBox picBoxGameDemo1;
  }
}
