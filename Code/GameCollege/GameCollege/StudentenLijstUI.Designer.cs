﻿namespace GameCollege
{
  partial class StudentenLijstUI
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StudentenLijstUI));
      this.label1 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.picBoxGameDemo1 = new System.Windows.Forms.PictureBox();
      ((System.ComponentModel.ISupportInitialize)(this.picBoxGameDemo1)).BeginInit();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(151, 107);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(206, 74);
      this.label1.TabIndex = 28;
      this.label1.Text = "(titel game)";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label3
      // 
      this.label3.Location = new System.Drawing.Point(139, 199);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(278, 73);
      this.label3.TabIndex = 37;
      this.label3.Text = resources.GetString("label3.Text");
      // 
      // picBoxGameDemo1
      // 
      this.picBoxGameDemo1.BackgroundImage = global::GameCollege.Properties.Resources.Bears_thumb_200x200_8767;
      this.picBoxGameDemo1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.picBoxGameDemo1.Location = new System.Drawing.Point(18, 186);
      this.picBoxGameDemo1.Name = "picBoxGameDemo1";
      this.picBoxGameDemo1.Size = new System.Drawing.Size(108, 108);
      this.picBoxGameDemo1.TabIndex = 30;
      this.picBoxGameDemo1.TabStop = false;
      // 
      // StudentenLijstUI
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.label3);
      this.Controls.Add(this.picBoxGameDemo1);
      this.Controls.Add(this.label1);
      this.Name = "StudentenLijstUI";
      this.Size = new System.Drawing.Size(488, 776);
      ((System.ComponentModel.ISupportInitialize)(this.picBoxGameDemo1)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.PictureBox picBoxGameDemo1;
    private System.Windows.Forms.Label label3;

  }
}
