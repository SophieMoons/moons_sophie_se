﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCollege
{
  public class GameDemoLijstController
  {
    protected GameDemoLijst mGameDemoLijst;
    protected GameDemoLijstUI view;
    private List<GameDemoController> gameDemoController = new List<GameDemoController>();

    public GameDemoLijstController(GameDemoLijst model)
    {
      mGameDemoLijst = model;
    }


    public GameDemoUI GetGameDemoUI(int index)
    {
      GameDemo g = mGameDemoLijst.GetGameDemo(index); //geeft null terug (waarom?)
      gameDemoController.Add(new GameDemoController(g));
      return gameDemoController[index].GetGameDemoUI(g);   
    }

    public GameDemoLijstUI GetGameDemoLijstUI(GameDemoLijst gameDemoLijstmodel) //voor GameCollege
    {
      GameDemoLijstUI gameDemoLijstUI = new GameDemoLijstUI(gameDemoLijstmodel, this);
      mGameDemoLijst.AddObserver(gameDemoLijstUI);
      return gameDemoLijstUI;
    }

    /*public void btnGameDemoClicked()
    {
      this.mGameDemoLijst.LaatStudentenZien(1); //wanneer geklikt -> GameDemoLijst niet zichtbaar
      gameDemoController[0].GameDemoClicked(); //eens zien of het werkt...
      //moet het invisible zien te krijgen...
    }*/
}
}
