﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameCollege
{
  public partial class GameDemoLijstUI : UserControl
  {
    private GameDemoLijst model;
    private GameDemoLijstController controller;
    private GameDemoUI[] gameDemoUI = new GameDemoUI[GameDemoLijst.AANTAL_GAMEDEMOS];

    public GameDemoLijstUI(GameDemoLijst model, GameDemoLijstController controller)
    {
      InitializeComponent();
      this.model = model;
      this.controller = controller;
      for (int i = 0; i < 3; i++)
      {
        gameDemoUI[i] = this.controller.GetGameDemoUI(i);
        gameDemoUI[i].Location = new System.Drawing.Point(10 + i * 164, 202);
        gameDemoUI[i].Size = new System.Drawing.Size(128, 128);
        this.Controls.Add(this.gameDemoUI[i]);
      }

      for (int i = 0; i < 3; i++)
      {
        gameDemoUI[i] = this.controller.GetGameDemoUI(i);
        gameDemoUI[i].Location = new System.Drawing.Point(10 + i * 164, 350);
        gameDemoUI[i].Size = new System.Drawing.Size(128, 128);
        this.Controls.Add(this.gameDemoUI[i]);
      }

      for (int i = 0; i < 3; i++)
      {
        gameDemoUI[i] = this.controller.GetGameDemoUI(i);
        gameDemoUI[i].Location = new System.Drawing.Point(10 + i * 164, 502);
        gameDemoUI[i].Size = new System.Drawing.Size(128, 128);
        this.Controls.Add(this.gameDemoUI[i]);
      }
      //UpdateUI();
    }


    /*public void UpdateUI()
    {
      controller.btnGameDemoClicked();
    }*/

    /*private void picBoxGameDemo_Click(object sender, EventArgs e)
    {
      controller.btnGameDemoClicked();
      this.Visible = false;
    }*/

  }
}