﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCollege
{
  public class GameCollegeController
  {
    protected GameCollege model;
    protected GameCollegeUI view;
    private List<GameDemoLijstController> gameDemoLijstController = new List<GameDemoLijstController>();
    private List<StudentenLijstController> studentenLijstController = new List<StudentenLijstController>();


    public GameCollegeController(GameCollege m)
    {
      model = m;
    }

    public GameDemoLijstUI GetGameDemoLijstUI(int index)
    {
      GameDemoLijst g = model.GetGameDemoLijst(index);
      gameDemoLijstController.Add(new GameDemoLijstController(g));
      return gameDemoLijstController[index].GetGameDemoLijstUI(g);    
    }

    public StudentenLijstUI GetStudentenLijstUI(int index)
    {
      StudentenLijst s = model.GetStudentenLijst(index);
      studentenLijstController.Add(new StudentenLijstController(s));
      return studentenLijstController[index].GetStudentenLijstUI(s);   
    }

    public GameCollegeUI GetGameCollegeUI()
    {
      view = new GameCollegeUI(model, this);
      return view;
    }
  }
}
