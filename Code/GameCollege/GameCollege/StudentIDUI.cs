﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameCollege
{
  public partial class StudentIDUI : UserControl
  {
    protected StudentID myStudentID;
    protected StudentIDController myStudentIDController;

    public StudentIDUI(StudentID s, StudentIDController sc)
    {
      InitializeComponent();
      myStudentID = s;
      myStudentIDController = sc;
      myStudentID.AddObserver(this);
    }

    public void UpdateUI()
    {
      lblVolledigeNaam.Text = myStudentID.GetVolledigeNaam();
      lblDevelopmentFunctie.Text= myStudentID.GetDevelopmentFunctie();
      lblLeeftijd.Text= Convert.ToString(myStudentID.GetLeeftijd());
    }

    /*public void DisableStemKnop()
    {
      btnStem.Enabled = false;
      myStudentID.Stem(); //gestemd = true
    }*/ //UpdateStemUI() doet beter

    private void btnStem_Click(object sender, EventArgs e)
    {
      myStudentIDController.btnStemClicked();
      btnStem.Enabled = myStudentID.Gestemd();
    }
  }
}
