﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCollege
{
  public class StudentenLijstController
  {

    protected StudentenLijst model;
    protected StudentenLijstUI view;

    private List<StudentIDController> studentIDControllers = new List<StudentIDController>();

    public StudentenLijstController(StudentenLijst g)
    {
      model = g;
    }

    public StudentIDUI GetStudentIDUI(int index)
    {
      StudentID s = model.GetStudentID(index);
      studentIDControllers.Add(new StudentIDController(s));
      return studentIDControllers[index].GetStudentIDUI(s);    
    }

    public StudentenLijstUI GetStudentenLijstUI(StudentenLijst studentenLijstmodel) //voor GameCollege
    {
      StudentenLijstUI studentenLijstUI = new StudentenLijstUI(studentenLijstmodel, this);
      model.AddObserver(studentenLijstUI);
      return studentenLijstUI;
    }

    /*public void btnTerugClicked()
    {
      this.model.TerugGaan();
      view.UpdateUI();
    }*/ 
  }
}
