﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCollege
{
  public class GameDemo
  {
    protected bool geklikt = false;
    protected List<GameDemoUI> observers = new List<GameDemoUI>();
    protected bool timeToSwitch = false;

    public GameDemo()
    { 
    }

    public bool OpGeklikt()
    {
      return geklikt;
    }

    public void LaatStudentenZien()
    {
      if (geklikt == false)
      {
        timeToSwitch = true;
      }
      NotifyObserver();
    }

    public void BlijfOpScherm()
    {
      if (geklikt == true)
      {
        timeToSwitch = false;
      }
      NotifyObserver();
    }

    public void AddObserver(GameDemoUI observer)
    {
      observers.Add(observer);
    }

    private void NotifyObserver() //als er variabele veranderen, dan worden de UI's refreshed
    {
      foreach (GameDemoUI gUI in observers)
      {
        gUI.Update();
      }
    }
  }
}
