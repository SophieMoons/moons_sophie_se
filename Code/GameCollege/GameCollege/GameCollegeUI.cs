﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameCollege
{
  public partial class GameCollegeUI : UserControl
  {
    private GameCollege model;
    private GameCollegeController controller;
    private GameDemoLijstUI[] gameDemoLijstUI = new GameDemoLijstUI[GameCollege.AANTAL_GAMEDEMOLIJSTEN];
    private StudentenLijstUI[] studentenLijstUI = new StudentenLijstUI[GameCollege.AANTAL_STUDENTENLIJSTEN];


    public GameCollegeUI(GameCollege model, GameCollegeController controller)
    {
      InitializeComponent();
      this.model = model;
      this.controller = controller;
      for (int i = 0; i < GameCollege.AANTAL_GAMEDEMOLIJSTEN; i++)
      {
        gameDemoLijstUI[i] = this.controller.GetGameDemoLijstUI(i);
        gameDemoLijstUI[i].Location = new System.Drawing.Point(5,5);
        this.Controls.Add(this.gameDemoLijstUI[i]);
      }

      for (int i = 0; i < GameCollege.AANTAL_STUDENTENLIJSTEN; i++)
      {
        studentenLijstUI[i] = this.controller.GetStudentenLijstUI(i);
        studentenLijstUI[i].Location = new System.Drawing.Point(5,5);
        this.Controls.Add(this.studentenLijstUI[i]);
      }
    }

    /*public void UpdateUI()
    {
      this.model.SwitchSchermen();
      gameDemoLijstUI[0].UpdateUI();
    }*/

    private void picGameDemo_Click(object sender, EventArgs e)
    {
      gameDemoLijstUI[0].Visible = false;
      btnTerug.Visible = true;
      picGameDemo.Visible = false;
    }

    private void btnTerug_Click(object sender, EventArgs e)
    {
      gameDemoLijstUI[0].Visible = true;
      btnTerug.Visible = false;
      picGameDemo.Visible = true;
    }
  }
}
