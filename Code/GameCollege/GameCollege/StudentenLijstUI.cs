﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameCollege
{
  public partial class StudentenLijstUI : UserControl
  {
    private StudentenLijst model;
    private StudentenLijstController controller;
    private StudentIDUI[] studentIDUI = new StudentIDUI[StudentenLijst.AANTAL_STUDENTEN];


    public StudentenLijstUI(StudentenLijst model, StudentenLijstController controller)
    {
      InitializeComponent();
      this.model = model;
      this.controller = controller;
      for (int i = 0; i < StudentenLijst.AANTAL_STUDENTEN; i++)
      {
        studentIDUI[i] = this.controller.GetStudentIDUI(i);
        studentIDUI[i].Location = new System.Drawing.Point(4,i*102+302);
        studentIDUI[i].Size = new System.Drawing.Size(416, 112);
        this.Controls.Add(this.studentIDUI[i]);
      }
      //UpdateUI();
    }


    /*public void UpdateUI()
    {

    }*/
  }
  }