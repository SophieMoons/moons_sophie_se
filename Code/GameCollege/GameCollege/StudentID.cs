﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCollege
{
  public class StudentID
  {
    protected static Random generator;
    protected string mVolledigeNaam, mDevelopmentFunctie; //verschillende naam en posities geven per studentID
    protected int mLeeftijd; //verschillende leeftijd geven per studentID
    protected bool erIsGestemd = false;
    protected bool enableButton = true;
    protected List<StudentIDUI> observers = new List<StudentIDUI>();

    protected int aantalStemmen=0;
    public const int MAX_AANTAL_STEMMEN = 3; 

    public StudentID()
    {
      if (generator == null)
      {
        generator = new Random();
      }
      mLeeftijd = generator.Next(18, 26);

      int caseSwitch = generator.Next(1, 7);
      switch (caseSwitch)
      {
        case 1:
          mVolledigeNaam = "Sarah Moermans";
          mDevelopmentFunctie = "GUI";
          break;
        case 2:
          mVolledigeNaam = "Jan Zonder Vrees";
          mDevelopmentFunctie = "Requirements";
          break;
        case 3:
          mVolledigeNaam = "Isabelle De Wreker";
          mDevelopmentFunctie = "Coding";
          break;
        case 4:
          mVolledigeNaam = "Karel de Gemiddelde";
          mDevelopmentFunctie = "Coding";
          break;
        case 5:
          mVolledigeNaam = "Sam de Vos";
          mDevelopmentFunctie = "Supervisor";
          break;
        case 6:
          mVolledigeNaam = "Aisha de Maesmaker";
          mDevelopmentFunctie = "Security";
          break;
        case 7:
          mVolledigeNaam = "Charel Poireau";
          mDevelopmentFunctie = "GUI";
          break;
        default:
          mVolledigeNaam = "Piet Snot";
          mDevelopmentFunctie = "Coding";
          break;
      }
    }

    public int GetLeeftijd()
    {
      return mLeeftijd;
    }

    public string GetVolledigeNaam()
    {
      return mVolledigeNaam;
    }

    public string GetNaam()
    {
      return mVolledigeNaam;
    }

    public string GetDevelopmentFunctie()
    {
      return mDevelopmentFunctie;
    }

    public bool Gestemd() //elke stem = button disabled en wanneer 3 keer gestemd, kan niet meer stemmen
    {
      return enableButton;
    }

    public void StemCheck()
    {
      if (!EndMaxStemmen())
      {
        if (erIsGestemd == false)
        {
          enableButton = true; //disable stemknop
        }

        else
        {
          enableButton = false;
          aantalStemmen++;
        }
      }
    }

    public Boolean EndMaxStemmen() //Checken
    {
      return aantalStemmen == MAX_AANTAL_STEMMEN;
    }

    /*public void Stem()
    {
      if (EndMaxStemmen())
      {
        //this.StemCheck();
        this.MaakLos();
      }
      else
      {
        //this.StemCheck();
        this.MaakVast();
        aantalStemmen++;
      }
      //this.StemCheck(); //nieuw
    }*/

    public void MaakLos()
    {
      erIsGestemd = false;
      //NotifyObserver();
      StemCheck();
    }

    public void MaakVast()
    {
      erIsGestemd = true;
      //NotifyObserver();
      StemCheck();
    }

    public void AddObserver(StudentIDUI observer)
    {
      observers.Add(observer);
    }

    /*private void NotifyObserver() //als er variabele veranderen, dan worden de UI's refreshed
    {
      //foreach (StudentIDUI sUI in observers)
      //{
      // sUI.UpdateUI();
      //} => als ik klik, dan verandert de case (mag ni gebeuren):I

      /*foreach (StudentIDUI sUI in observers)
      {
       this.StemCheck();
      }*/
    }
  }
