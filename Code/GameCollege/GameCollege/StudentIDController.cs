﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCollege
{
  public class StudentIDController
  {
    protected StudentID mStudentID;

    public StudentIDController(StudentID sI)
    {
      mStudentID = sI;
    }

    public StudentIDUI GetStudentIDUI(StudentID stID)
    {
      StudentIDUI studentIDUI = new StudentIDUI(stID, this);
      mStudentID.AddObserver(studentIDUI);
      studentIDUI.UpdateUI();
      return studentIDUI;
    }


    public void btnStemClicked() //om knop te disablen na stemmen
    {
      if (this.mStudentID.Gestemd())
      {
        this.mStudentID.MaakVast();
      }
      else
      {
        this.mStudentID.MaakLos();
      }
    }
  }
}
