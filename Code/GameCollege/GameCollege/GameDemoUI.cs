﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameCollege
{
  public partial class GameDemoUI : UserControl
  {
    protected GameDemo myGameDemo;
    protected GameDemoController myGameDemoController;
    protected Image gameImage;

    public GameDemoUI(GameDemo g, GameDemoController gc)
    {
      InitializeComponent();
      myGameDemo = g;
      myGameDemoController = gc;
      myGameDemo.AddObserver(this);
    }
  }
}
